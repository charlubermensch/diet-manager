# Diet Manager

Commands with managing a diet as purpose.

# Documentation

The repo contains a man page `diet.man`.

It can be seen on Unix-based system with the command:

```
groff -Tascii -man diet.man | less
```

# Commands

- nutrients -- show the nutrients given by a diet
- needs -- show the nutrients needed

# Dependencies

 Python only as of now.

# Examples

## diet nutrients

```sh
python3 . nutrients
```

Depends upon `diet.txt` and `src/food_data.txt`.

`src/food_data.txt` doesn't require to be change unless you want to add food information to the database.

`diet.txt` must be change according to your diet with this format:

```
<quantity> <food name>
<quantity> <food name>
```

<quantity> is in gram.

## diet needs

```sh
python3 . needs src/info.txt
```

Depends upon `src/info.txt`. You **must** edit this file in order that the output is correct:

```
sex <(wo)man>
age <age in years>
height <height in centimeters>
weight <weight in kilograms>
activity <activity in float>
```

Basically, the software calculate your [BMR](https://en.wikipedia.org/wiki/Basal_metabolic_rate) and then multiply it by your activity ratio.

Activity is a number between 1 and 2.

1.4 Sedentary: little or no exercise <br>
1.5 Exercise 1-3 times/week <br>
1.6 Exercise 4-5 times/week <br>
1.7 Daily exercise or intense exercise 3-4 times/week <br>
1.9 Intense exercise 6-7 times/week <br>
2.1 Very intense exercise daily, or physical job <br>

# Roadmap

- [x] `nutrients` command
- [x] `needs` command
- [x] better documentation
- [ ] support for Windows broken return line
- [ ] `add` command -- in order to add nutrients of food onto food database from web
- [ ] `check` command -- check the difference between `nutrients` output and `needs` one
- [ ] switching to ncurses

# License

The license is the Gnu General License Version 2. This code shouldn't be used in proprietary software.