diet needs <info file>

Display the needs of a person according to a set of information.

<info file> is a file with all the info required in order to calculate the needs

sex (wo)man
age <integer>
weight <integer in cm>
height <integer in cm>
activity <float>

-V | --version -> Display software version information.
-h | --help    -> Display this information.
