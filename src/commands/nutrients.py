"""Check the nutrients of a diet according to a database."""

import sys


def get_content(file_path: str) -> str:
    """Return content of `file_path`."""
    with open(file_path, "rb") as file:
        b_file = file.read()
    return str(b_file, encoding="utf-8")


def get_data(line: str, i: int) -> tuple:
    """Return data from `line`."""
    if not line.strip():
        return ()

    if " " not in line:
        print("error line %d: space not found" % i, file=sys.stderr)
        sys.exit(1)

    return (line[:line.index(" ")], line[line.index(" ")+1:])


def get_nice_float(s: str) -> float:
    """Return a float with less than 2 figures after the point."""
    f = float(s)
    return int(f*100)/100


def get_food_data() -> dict:
    """Return food data from `food_data.txt`."""
    food_data = {}
    food_buffer = {}

    lines = get_content("src/food_data.txt").split("\n")
    for i, line in enumerate(lines):
        data = get_data(line, i)
        if not data:
            continue

        if data[0] == "name":
            if food_buffer:
                name = food_buffer["name"]
                del food_buffer["name"]
                food_data[name] = food_buffer
                food_buffer = {}

            food_buffer[data[0]] = data[1]
            continue

        food_buffer[data[0]] = get_nice_float(data[1])

    if food_buffer:
        name = food_buffer["name"]
        del food_buffer["name"]
        food_data[name] = food_buffer
        food_buffer = {}

    return food_data


def check_argv(argv: list):
    """Check `argv` for arguments."""
    if len(argv) == 1:
        return

    if argv[1] in ("-h", "--help"):
        with open("src/help/nutrients.txt", "rb") as file:
            b_file = file.read()
        s_file = str(b_file, encoding="utf-8")
        print(s_file, end="")
        sys.exit(0)

    if argv[1] in ("-V", "--version"):
        print("Diet Manager's nutrients command beta")
        sys.exit(0)


def get_parsed_diet(line: str, i: int) -> tuple:
    """Return quantity and food name contained in `line`."""
    if " " not in line:
        print("error line %d: space not found" % i, file=sys.stderr)
        sys.exit(1)

    s_quantity = line[:line.index(" ")]

    try:
        quantity = int(s_quantity)
    except ValueError:
        err_msg = "error line %d: \"%s\" is not an integer" % (i, s_quantity)
        print(err_msg, file=sys.stderr)
        sys.exit(1)

    return (quantity, line[line.index(" ")+1:])


def get_diet_nutrients(food_data: dict) -> dict:
    """Return diet nutrients."""
    diet_nutrients = {}

    diet = get_content("diet.txt").split("\n")[:-1]
    for i, line in enumerate(diet):
        quantity, food_name = get_parsed_diet(line, i)
        food = food_data[food_name]
        for nutrient in food:
            if nutrient not in diet_nutrients:
                diet_nutrients[nutrient] = 0
            diet_nutrients[nutrient] += food[nutrient] * quantity / 100

    return diet_nutrients


def get_nutrient_value(diet_nutrients: dict, key: str, argv: list) -> float:
    """Return what will be shown as dayly quantity of `key`."""
    if "--week" in argv:
        return get_nice_float(diet_nutrients[key] / 7)

    return get_nice_float(diet_nutrients[key])


def run(argv: list):
    """Check the nutrients of a diet according to a database."""
    check_argv(argv)

    food_data = get_food_data()
    diet_nutrients = get_diet_nutrients(food_data)
    for key in diet_nutrients:
        print(key, get_nutrient_value(diet_nutrients, key, argv))


if __name__ == "__main__":
    run(sys.argv)
