"""Calculate the needs of an individual."""

import sys

from typing import Union


def get_kcal(sex: str, weight: int, height: int, age: int) -> int:
    """Return calories according to `sex`, `weight`, `height` and `age`."""
    if sex == "man":
        return 10 * weight + 6.25 * height - 5 * age + 5
    return 10 * weight + 6.25 * height - 5 * age - 161


def get_kcal_from_data(data: dict) -> int:
    """Return calories according to `data`."""
    raw_kcal = get_kcal(
        data["sex"],
        data["weight"],
        data["height"],
        data["age"]
    )

    kcal = raw_kcal * data["activity"]

    return kcal


def get_content(input_file: str) -> str:
    """Return content of `input_file`."""
    with open(input_file, "rb") as file:
        b_file = file.read()
    return str(b_file, encoding="utf-8")


def get_value(s: str) -> Union[str, list]:
    """Return the parsed content of `s`."""
    if s.count(" ") == 0:
        return s
    return s.split(" ")


def get_parsed_content(input_file: str) -> dict:
    """Return the content of `input_file` as `dict`."""
    parsed_content = {}
    lines = get_content(input_file).split("\n")
    for i, line in enumerate(lines):
        if not line.strip():
            continue

        if " " not in line:
            msg_err = "error line %d: syntax error" % i
            print(msg_err, file=sys.stderr)
            print(f"{line=}")
            sys.exit(1)

        key = line[:line.index(" ")]
        value = get_value(line[line.index(" ")+1:])
        parsed_content[key] = value

    return parsed_content


def get_nutrient_value(nutrient: str, data: dict) -> float:
    """Return the need in `nutrient` according to `data`."""
    nutrients = get_parsed_content("src/needs.txt")
    values = nutrients[nutrient]
    # teen man woman sporty

    if data["activity"] > 1.5:
        return float(values[-1])

    if data["age"] < 15:
        return float(values[0])

    if data["sex"] == "woman":
        return float(values[2])

    return float(values[1])

    print(f"{data=}")

    sys.exit(0)


def get_nutrient(nutrient: str, data: dict) -> tuple:
    value = get_nutrient_value(nutrient, data)
    return (nutrient, value)


def get_nutrients() -> list:
    """Return the list of nutrients in `needs.txt`."""
    lines = get_content("src/needs.txt").split("\n")
    return [line.split(" ")[0] for line in lines if line.strip()]


def get_info_from_data(data: dict) -> tuple:
    """Return needs according to `data`."""
    info = (("kcal", get_kcal_from_data(data)),)

    for nutrient in get_nutrients():
        info += (get_nutrient(nutrient, data),)

    return info


def get_clean_data(dirty_data: dict) -> dict:
    """Return "clean" data by converting some `str` to `int`."""
    clean_data = {
        "sex": dirty_data["sex"],
        "activity": float(dirty_data["activity"])
    }

    for e in ("age", "height", "weight"):
        clean_data[e] = int(dirty_data[e])

    return clean_data


def get_info_from_info(info_file: str) -> tuple:
    """Return the needs according to `info_file` content."""
    dirty_data = get_parsed_content(info_file)
    data = get_clean_data(dirty_data)
    return get_info_from_data(data)


def check_argv(argv: list):
    """Check `argv` for arguments."""
    if len(argv) == 1:
        print("error: no input file", file=sys.stderr)
        sys.exit(1)

    if argv[1] in ("-h", "--help"):
        with open("src/help/needs.txt", "rb") as file:
            b_file = file.read()
        s_file = str(b_file, encoding="utf-8")
        print(s_file, end="")
        sys.exit(0)

    if argv[1] in ("-V", "--version"):
        print("Diet Manager's needs command beta")
        sys.exit(0)


def run(argv: list):
    """Calculate the needs of an individual."""
    check_argv(argv)

    input_file = argv[1]
    info = get_info_from_info(input_file)
    for data in info:
        print(data[0], data[1])


if __name__ == "__main__":
    run(sys.argv)
