.TH DIET 7 "25 January 2022"
.SH NAME
diet \- Commands with managing diets as purpose
.SH SYNOPSIS
.LP
python . [command] [[--commands's options]
.SH COMMANDS
.TP
.B nutrients
Show nutrients from a diet.

Depends upon
.I diet.txt
and
.I src/food_data.txt
.I src/food_data.txt
doesn't require to be changed unless you want to add food information to the database.
.I diet.txt
must be changed according to your diet with this format: (quantity is in integer gram)

.RS
.RS
<quantity> <food_name>
.br
<quantity> <food_name>
.RE
.RE

.TP
.B needs
.I src/info.txt
.br
Depends upon
.I src/info.txt

You must edit this file in order that the output is of any use.
This file is of this format:

.RS
.RS
sex <(wo)man>
.br
age <age in years>
.br
height <height in centimers>
.br
weight <weight in centimers>
.br
activity <activity in float>
.RE
.RE

Basically the software calculate your BMR and then multiply it by your activity ratio to get your daily calories need.
.SH EXAMPLES
.TP
python . nutrients
.TP
python . needs
.SH "REPORTING BUGS"
GitLab issues: <https://gitlab.com/charlubermensch/diet-manager/-/issues>
.SH BUGS
ValueError usually comes from a non-number entry where there should be one. Double check the file you edited. You may also convert your float into integer.
.SH "SEE ALSO"
python(1)
.SH AUTHOR
Written by "Charl'Ubermensch" (pseudonyme).
.LP
.UR https://gitlab.com/charlubermensch
Remote Git repositories
.UE
.LP
.MT inlife.developper@tutanota.com
CharlUbermensch
.ME
.br
.MT contact@charlubermensch.com
CharlUbermensch
.ME
.SH COPYRIGHT
License GPLv2: GNU General Public License version 2.
<https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
