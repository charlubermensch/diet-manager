"""Launch a sub-command."""

import sys

from src.commands.nutrients import run as nutrients_run
from src.commands.needs import run as needs_run


def check_argv(argv: list):
    """Check `argv` for arguments."""
    if len(argv) == 1:
        print("error: missing command", file=sys.stderr)
        sys.exit(1)

    if argv[1] in ("-h", "--help"):
        with open("src/help/main.txt", "rb") as file:
            b_file = file.read()
        s_file = str(b_file, encoding="utf-8")
        print(s_file, end="")
        sys.exit(0)

    if argv[1] in ("-V", "--version"):
        print("Diet Manager alpha")
        sys.exit(0)


def run(argv: list):
    """Launch a sub-command."""
    check_argv(argv)

    command = argv[1]
    command_to_function = {
            "nutrients": nutrients_run,
            "needs": needs_run
    }

    if command not in command_to_function.keys():
        msg_err = "error: \"%s\" is not a Diet Manager command" % command
        print(msg_err, file=sys.stderr)
        sys.exit(1)

    command_run_function = command_to_function[command]
    command_run_function(argv[1:])


if __name__ == "__main__":
    run(sys.argv)
